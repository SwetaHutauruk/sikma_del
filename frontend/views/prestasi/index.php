<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\DetailView;

?>
<div class="panel-body">
<table class="table table-hover" id="myTable">
  <thead>
    <tr>
      <th scope="col">Jenis Prestasi</th>
      <th scope="col">Tanggal Awal Kegiatan</th>
      <th scope="col">Tanggal Akhir Kegiatan</th>
      <th scope="col">Keterangan</th>
      <th scope="col">Status</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <?php foreach ($prestasi as $prestasi): ?>
  <tr>
  
      <td><?=$prestasi->jenis_prestasi?></td>
      <td><?=$prestasi->tgl_awal_kegiatan?></td>
      <td><?=$prestasi->tgl_akhir_kegiatan?></td>
      <td><?=$prestasi->keterangan?></td>
      <td><?php if($prestasi->status_request_id == 0):?>
      <a class="label label-warning">Menunggu</a>
      <?php elseif($prestasi->status_request_id == 1):?>
      <a class="label label-success">Disetujui</a>
      <?php endif ?>
      </td>
       <td>
        <?= Html::a('View', ['view', 'id_prestasi','url' ,'view', 'id'=> $prestasi->id_prestasi], ['class' => 'btn btn-primary']) ?>
      <?php if($prestasi->status_request_id != 1):?>
        <?= Html::a('Edit', ['update', 'id_prestasi','url' ,'view', 'id'=> $prestasi->id_prestasi], ['class' => 'btn btn-warning']) ?>
      <?php endif ?>
      <!-- <a href="#" class="btn btn-danger btn-sm">Hapus</a> -->
      </td> 
    </tr> 
    <?php endforeach; ?>
</table>
</div>


<script src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<script>
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>   



