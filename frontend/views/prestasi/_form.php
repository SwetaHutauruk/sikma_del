<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Prestasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prestasi-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'jenis_prestasi')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'tgl_awal_kegiatan')->widget(DatePicker::className(), [
           'options' => ['placeholder' => 'Masukkan tanggal'],
           'pluginOptions' => [
               'autoclose' => true,
               'format' => 'yy/mm/dd'
           ]
           
       ]);?>

<?= $form->field($model, 'tgl_akhir_kegiatan')->widget(DatePicker::className(), [
           'options' => ['placeholder' => 'Masukkan tanggal'],
           'pluginOptions' => [
               'autoclose' => true,
               'format' => 'yy/mm/dd'
           ]
           
       ]);?>




    <?= $form->field($model, 'upload_file')->fileInput() ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Request', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
