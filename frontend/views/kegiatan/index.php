<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KegiatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="panel-body">
<table class="table table-hover" id="myTable">
  <thead>
    <tr>
    <th scope="col">Jenis Kegiatan</th>
    <!-- <th scope="col">Upload Proposal</th> -->
    <th scope="col">Status</th>
    </tr>
    </thead>
    <?php foreach($kegiatan as $kegiatan):?>
    <tr>

    <td><?=$kegiatan->jenis_kegiatan?></td>
    <td><?php if($kegiatan->status_request_id == 0):?>
      <a class="label label-warning">Menunggu</a>
      <?php elseif($kegiatan->status_request_id == 1):?>
      <a class="label label-success">Disetujui</a>
      <?php endif ?>
      </td>
       <td>
       
       <?= Html::a('View', ['view', 'id_kegiatan','url' ,'view', 'id'=> $kegiatan->id_kegiatan], ['class' => 'btn btn-primary']) ?>
      <?php if($kegiatan->status_request_id != 1):?>
        <?= Html::a('Edit', ['update', 'id_kegiatan','url' ,'view', 'id'=> $kegiatan->id_kegiatan], ['class' => 'btn btn-warning']) ?>
      <?php endif ?>
      <!-- <a href="#" class="btn btn-danger btn-sm">Hapus</a> -->
      </td> 
    </tr> 
    <?php endforeach; ?>
</table>
</div>
