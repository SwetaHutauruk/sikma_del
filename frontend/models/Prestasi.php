<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prestasi".
 *
 * @property int $id_prestasi
 * @property string $jenis_prestasi
 * @property string $tgl_awal_kegiatan
 * @property string $tgl_akhir_kegiatan
 * @property string $upload_file
 * @property string $keterangan
 */
class Prestasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prestasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_prestasi',  'tgl_awal_kegiatan', 'tgl_akhir_kegiatan', 'upload_file', 'keterangan'], 'required'],
            [[ 'tgl_awal_kegiatan', 'tgl_akhir_kegiatan'], 'safe'],
            [['jenis_prestasi',  'keterangan'], 'string', 'max' => 255],
           
            [['upload_file'], 'file','skipOnEmpty'=>TRUE,'extensions'=>'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_prestasi' => 'Id Prestasi',
            'jenis_prestasi' => 'Jenis Prestasi',
            'tgl_awal_kegiatan' => 'Tgl Awal Kegiatan',
            'tgl_akhir_kegiatan' => 'Tgl Akhir Kegiatan',
            'upload_file' => 'Upload File',
            'keterangan' => 'Keterangan',
           
        ];
    }
}
