<?php

namespace app\models;

use yii\base\Model;
use yii\UploadFile;
use app\models\Prestasi;

class UploadForm extends Prestasi
{
    public $nama_file;


    const IS_REQUIRED = 'is_required';
    const NOT_REQUIRED = 'not_required';


    public function rules(){
        return[
            [['upload_file'],'file', 'skipOnEmpty'=> false, 'extensions'=> 'pdf', 'maxSize' => 100000, 'tooBig' => 'Ukuran File Maksimum 10MB', 'on' => self::IS_REQUIRED , 'message'=>'Tidak boleh kosong'],
            [['upload_file'],'file', 'skipOnEmpty'=> true, 'extensions'=> 'pdf', 'maxSize' => 100000, 'tooBig' => 'Ukuran File Maksimum 10MB', 'on' => self::NOT_REQUIRED],
        ];
    }

    public function upload($baseDir)
    {
        if ($this->validate()){
            $fileName = $baseDir.'/' . $this->nama_file->baseName . '.' .$this->nama_file->extension;
            $this->nama_file->saveAs($fileName);
            return $fileName;
        } else {
            $errors = $this->getErrors();
            var_dump($this->validate());
            die();
            return null;
        }
    }

    public function attributeLabels(){
        return [
            'upload_file' => 'File',

        ];
    }
}