<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "kegiatan".
 *
 * @property int $id_kegiatan
 * @property string $jenis_kegiatan
 * @property string $upload_proposal
 * @property string $upload_lpj
 */
class Kegiatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kegiatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_kegiatan', 'upload_proposal'], 'required'],
            [['jenis_kegiatan'], 'string', 'max' => 255],
            [['upload_proposal'], 'file','skipOnEmpty'=>TRUE,'extensions'=>'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kegiatan' => 'Id Kegiatan',
            'jenis_kegiatan' => 'Jenis Kegiatan',
            'upload_proposal' => 'Upload Proposal',
        ];
    }
}
